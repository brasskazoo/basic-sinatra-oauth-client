# Basic Sinatra OAuth Client

A basic template for an OAuth version 1 client using Sinatra for rapid prototyping.

The two-step authorisation process is handled by a single `/oauth` URL, which also acts as the callback point.

## Setup
* Install the appropriate gems (`sinatra` and `oauth`)

You will need to change the following (notated by comments in the source):

* Configure the consumer key and secret (provided by the OAuth provider) 
* Replace the target site URL
* Replace the target endpoint

## Usage
* Run with `ruby client.rb`
* Open `http://localhost:4567/status` to kick off the process