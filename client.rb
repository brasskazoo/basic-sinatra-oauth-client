require 'rubygems'
require 'sinatra'
require 'oauth'

## Sinatra Config
enable :sessions

# Constants
CONSUMER_KEY = ""     ## Replace with real value
CONSUMER_SECRET = ""  ## Replace with real value

get "/oauth" do
  if params[:oauth_verifier]
    access = session[:auth_request].get_access_token :oauth_verifier => params[:oauth_verifier]

    session[:auth_token] = access
    ## Could get access token + secret at this stage
    #session[:oauth_access_token] = access.token
    #session[:oauth_access_token_secret] = access.secret

    redirect "/status"
  else
    consumer = OAuth::Consumer.new CONSUMER_KEY, CONSUMER_SECRET, :site => "http://example.com" ## Replace with target server
    auth_request = consumer.get_request_token

    session[:auth_request] = auth_request
    redirect auth_request.authorize_url
  end
end

get "/status" do
  unless session[:auth_token]
    redirect "/oauth"
  end

  response = session[:auth_token].get '/api/endpoint' ## Replace with proper service
  response.body
end
